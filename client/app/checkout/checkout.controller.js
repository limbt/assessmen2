(function (){
    angular
        .module("FGM")
        .controller("CheckoutCtrl", CheckoutCtrl);

   CheckoutCtrl.$inject = ['$state', 'DeptService'];

   function CheckoutCtrl($state, DeptService){
        var vm = this;

        vm.groceriesPick = groceriesPick;
        vm.groceries = [];
        vm.updateRec = updateRec;
        
        var groceriesID = DeptService.extract();

        console.log( "at checkoutcontroller %d", vm.groceriesID);
        
        groceriesPick(groceriesID);
               
        function groceriesPick(gID){
            DeptService.retrieveDeptByID(gID)
                .then(function (results){
                    vm.groceries  = results.data;
                    console.log("At Search Controller %s" , JSON.stringify(vm.groceries))
                })
                .catch(function (err){
                    console.log("error " + err);
                });
        }

               

         function searchFromDB(param){
            DeptService
                .retrieveDeptDB(param)
                .then(function (results){
                    vm.groceries  = results.data;
                    vm.groceriesQty = results.data.length;
                    console.log("At Search Controller %s" , vm.groceries)
                })
                .catch(function (err){
                    console.log("error " + err);
                });
        }

         function updateRec(id){
            console.log("Update Selected item....."+ id);
            DeptService
                .updateDept(id, vm.groceries)
                .then(function(response){
                    console.log(JSON.stringify(response.data));
                    console.log(JSON.stringify(vm.status.code));
                }).catch(function(err){
                    //  console.log("error " + JSON.stringify(err));
                    // vm.status.message = err.data.name;
                    // vm.status.code = err.data.parent.errno;
                    // console.log(vm.status.code, vm.status.message)

                });
        }
               


        vm.gonext = gonext;

        function gonext(){
            $state.go('A');
        }

   }

})();