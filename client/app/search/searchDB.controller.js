(function (){
    angular
        .module("FGM")
        .controller("SearchDBCtrl", SearchDBCtrl);

   SearchDBCtrl.$inject = ['$state', 'DeptService'];

   function SearchDBCtrl($state, DeptService){
        var vm = this;

        vm.searchString = '';
        vm.result = null;
        vm.showManager = false;
        vm.search = search;
        vm.deleteRec = deleteRec;
        vm.groceries = [{"id": "", "upc12": "", "brand" : "", "name" : ""}];
        vm.groceriesQty = 0;
        vm.select = select;
        vm.groceriesPick = [];
        
        var id = 0;
        init();

        function init(){

           searchFromDB('mama');
        }

        function search(){
            vm.showManager = false;
            searchFromDB(vm.searchString);
        }

        
        function searchFromDB(param){
            DeptService
                .retrieveDeptDB(param)
                .then(function (results){
                    vm.groceries  = results.data;
                    vm.groceriesQty = results.data.length;
                    console.log("At Search Controller %s" , vm.groceries)
                })
                .catch(function (err){
                    console.log("error " + err);
                });
        }

        function deleteRec(no){
            console.log("deleting record no %s", no );
            DeptService
                .deleteDept(no)
                .then(function (results){
                    vm.groceries  = results.data;
                    init();
                })
                .catch(function (err){
                    console.log("error " + err);
                });
        }

        vm.gonext = gonext;

        function gonext(){
               $state.go('D');
        }
       
        function select(id){
            console.log(id);
            console.log( "Selecting" + id);
            // vm.groceriesPick.push(vm.groceries);
            // console.log("Current picked item are: ", JSON.stringify(vm.groceriesPick));
            DeptService
                .transfer(id);    
        }

   }

})();