(function () {
    angular
        .module("FGM")
        .config(uirouterAppConfig);
    uirouterAppConfig.$inject = ["$stateProvider","$urlRouterProvider"];

    function uirouterAppConfig($stateProvider,$urlRouterProvider){
        $stateProvider
            .state('A',{
                url : '/A',
                templateUrl: "pages/searchDB.html",
                controller : 'SearchDBCtrl',
                controllerAs : 'ctrl'
            })
             .state("D", {
                url: "/D",
                templateUrl: "pages/checkout.html"
            })


        $urlRouterProvider.otherwise("/A");
    }

})();