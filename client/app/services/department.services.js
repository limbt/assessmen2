(function(){
    angular
        .module("FGM")
        .service("DeptService", DeptService);

    DeptService.$inject = ['$http'];

    function DeptService($http){
        var service = this;
        service.insertDept = insertDept;
        service.retrieveDeptDB = retrieveDeptDB;
        service.deleteDept = deleteDept;
        service.updateDept = updateDept;
        service.retrieveDeptByID = retrieveDeptByID;
        service.transfer = transfer;
        service.temp = [];
        service.extract = extract;
        service.groceriesID = 0;
        var groceries = [];

        function insertDept(groceries){
            return $http({
                method: 'POST',
                url: "api/groceries",
                data: {groceries}            });
        }

        function retrieveDeptDB(searchString){
            return $http({
                method: 'GET',
                url: "api/groceries",
                params: {'searchString': searchString}
            });
        }

        function deleteDept(id){
            return $http({
                method: 'DELETE',
                url: "/api/groceries/" +id
            });
        }

        
        function updateDept(id, groceries){
            
            console.log( id, JSON.stringify(groceries));
           
            return $http({
                method: 'PUT',
                url: "/api/groceries/" + id,
                data: {                    
                    brand: groceries[0].brand,
                    name: groceries[0].name,
                    upc12: groceries[0].upc12
                }
            });
        }

        function transfer(groceriesID){

            service.temp = groceriesID;
        };

        function extract(){
            return service.temp;
        };



        function retrieveDeptByID(ID){
            console.log(ID);
            return $http({
                method: 'GET',
                url: "api/groceries/?searchString=" +ID
            });
        }
    }    

})();