

module.exports = function (sequelize, Sequelize) {
    var TableMod = sequelize.define("groceries",
        {
            id: {
                type: Sequelize.INTEGER(11),
                primaryKey: true,
                allowNull: false,
                autoIncrement: true      
            },
            upc12: {
                type: Sequelize.BIGINT(12),
                allowNull: false
            },
            brand: {
                type: Sequelize.STRING,
                allowNull: true
            },
            name: {
                type: Sequelize.STRING,
                allowNull: true
            }

        },
        {
            // don't add timestamps attributes updatedAt and createdAt
            timestamps: false
         });

    return TableMod;
};
