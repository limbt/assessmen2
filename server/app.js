var express = require("express");
var path = require("path");
var bodyParser = require("body-parser");
var Sequelize = require("sequelize")

const NODE_PORT = process.env.NODE_PORT || 3000;

const CLIENT_FOLDER = path.join(__dirname + '/../client');  // CLIENT FOLDER is the public directory
const MSG_FOLDER = path.join(CLIENT_FOLDER + '/assets/messages');
const API_PROJ_ENDPOINT = "/api/groceries";

// Defines MySQL configuration
const MYSQL_USERNAME = 'limbte';
const MYSQL_PASSWORD = 'b0224t';

var app = express();

// Creates a MySQL connection
var sequelize = new Sequelize(
    'project',
    MYSQL_USERNAME,
    MYSQL_PASSWORD,
    {
        host: 'localhost',         // default port    : 3306
        logging: console.log,
        dialect: 'mysql',
        pool: {
            max: 5,
            min: 0,
            idle: 10000
        }
    }
);

// Loads model for database table
var TableMod = require('./models/table')(sequelize, Sequelize);

app.use(express.static(CLIENT_FOLDER));
app.use(bodyParser.json());

// Defines endpoint handler exposed to client side for registration
app.post("/api/groceries", function (req, res) {
    // Information sent via an HTTP POST is found in req.body
    console.log('\n At Server POST Data Submitted');
    console.log('id: ' + req.body.groceries.id);
    console.log('upc12: ' + req.body.groceries.upc12);
    console.log('brand: ' + req.body.groceries.brand);
    console.log('name: ' + req.body.groceries.name);


    TableMod
        .create({
            id: req.body.groceries.id,
            upc12: req.body.groceries.upc12,
            brand: req.body.groceries.brand,
            name: req.body.groceries.name
            
        })
        .then(function (result) {
            console.log("At Server POST then >>> " + result.get({plain: true}));
            res
                .status(200)
                .json(result);
        })
        .catch(function (err) {
            console.log("error: " + err);
            res
                .status(500)
                .json(err);
        });
    });

// Defines endpoint handler exposed to client side for retrieving department information from database
app.get("/api/groceries", function (req, res) {
    TableMod
    // findAll asks sequelize to search
        .findAll({
            where: {
                // This where condition filters the findAll result so that it only includes department names and
                // department numbers that have the searchstring as a substring (e.g., if user entered 's' as search
                // string, the following
                $or: [
                    {brand: {$like: "%" + req.query.searchString + "%"}},
                    {name: {$like: "%" + req.query.searchString + "%"}},
                    {id: {$like: "%" + req.query.searchString + "%"}},
                    
                ]
            }
        })
        .then(function (groceries) {
            console.log("At Server GET success and result is %s", groceries);
            res
                .status(200)
                .json(groceries);
        })
        .catch(function (err) {
            console.log("At server GET err ");
            res
                .status(500)
                .json(err);
        });
    });

// app.get("/api/groceries/managers", function (req, res) {
//     TableMod
//         .findAll({
//             where: {
//                 // $or operator tells sequelize to retrieve record that match any of the condition
//                 $or: [
//                     // $like + % tells sequelize that matching is not a strict matching, but a pattern match
//                     // % allows you to match any string of zero or more characters
//                     {brand: {$like: "%" + req.query.searchString + "%"}},
//                     {name: {$like: "%" + req.query.searchString + "%"}}
//                     ]
//                    }
//             })
                      
//         .then(function (groceries) {
//             res
//                 .status(200)
//                 .json(groceries);
//         })
//         // this .catch() handles erroneous findAll operation
//         .catch(function (err) {
//             res
//                 .status(500)
//                 .json(err);
//         });
// });

app.get(API_PROJ_ENDPOINT + "/:id", function(req, res){
    console.log(req.params.id);
    TableMod
            .find({
                where: { 
                    id: req.params.id
                }
            }).then(function(groceries){
                res
                    .status(200)
                    .json(groceries);
            }).catch(function(err){
                res
                    .status(500)
                    .json(err);
            })
})

app.put(API_PROJ_ENDPOINT + "/:id", function(req, res){
    var whereClause = {};
    whereClause.id = req.params.id;
    
    TableMod
        .update({ name: req.body.name, brand: req.body.brand},
            {where:  whereClause}
        ).then(function(result){
            res
                .status(200)
                .json(result);
        }).catch(function(err){
            console.log(err);
            res
                .status(500)
                .json(err);
        })
});

app.delete(API_PROJ_ENDPOINT + "/:id", function(req, res){
    console.log("delete groceries with id as:");
    console.log(req.params.id);
    var whereClause = {};
    whereClause.id = req.params.id;
       
    TableMod
        .destroy({
            where: whereClause
        }).then(function (result){
            console.log(result);
            if(result > 0){
                res.status(200).json({success: true});
            }else{
                res.status(200).json({success: false});
            }
        }).catch(function(err){
            res.status(500).json(err);
        });
});

app.use(function (req, res) {
    res.status(404).sendFile(path.join(MSG_FOLDER + "/404.html"));
});

app.use(function (req, res) {
    res.status(404).sendFile(path.join(MSG_FOLDER + "/404.html"));
});

app.listen(NODE_PORT, function () {
    console.log("Server running at http://localhost:" + NODE_PORT);
});